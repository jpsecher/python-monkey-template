import pytest
from unittest.mock import call
from urllib.parse import urlparse
import datetime
import datadog

from helpers.testhelpers import assert_items_equal, parse_as_json, MockDatetime


def test__running(client):
    # -------------------------------------------------------------------------
    res = client.get('/monitor/running')
    # -------------------------------------------------------------------------
    assert parse_as_json(res) == {'data': True}
    assert res.status_code == 200


def test__send_stats_ok_initially(client, application, mocker):
    mocker.patch('datadog.statsd.service_check')
    # -----------------------------------------------------------------------
    application._send_stats()
    # -----------------------------------------------------------------------
    datadog.statsd.service_check.assert_called_once_with(
        'monkey.template', datadog.statsd.OK)


def test__no_result_initially(client):
    # -----------------------------------------------------------------------
    response = client.get('/list/result')
    # -----------------------------------------------------------------------
    res = parse_as_json(response)
    assert res['data'] == []
    assert response.status_code == 200
