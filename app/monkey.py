from flask import Flask, jsonify, redirect, url_for
from threading import Thread
from os import getenv
from time import sleep
import logging
import datadog

from helpers.helpers import now_as_iso9601


class App(Thread):
    """Encapsulates a worker thread and a webserver.

    The worker thread detects problems.

    The webserver can be queried for the results and act as a monitoring
    endpoint.
    """

    def __init__(self):
        self._all_logs_to_stdout_using_flask_default_log()
        self._webserver = Flask(__name__)
        self._set_up_routes()
        self._last_update_datetime = now_as_iso9601()
        self._reset()
        super(App, self).__init__(name='webserver')

    def run(self):  # pragma: no cover
        while True:
            self._update()
            sleep(self._seconds_between_updates())

    def get_webserver(self):
        return self._webserver

    # ----------------------------------------------------------------------

    @staticmethod
    def _seconds_between_updates():
        return 300  # 5 minutes

    @staticmethod
    def _datadog_topic():
        return 'monkey.template'

    def _reset(self):
        "Ease testing"
        self._errors = []
        self._set_up_datadog()

    def _update(self):
        # TODO: make dummy request to set _errors.
        self._send_stats()

    def _send_stats(self):
        self._last_update_datetime = now_as_iso9601()
        topic = self._datadog_topic()
        if self._errors:
            return datadog.statsd.service_check(topic, datadog.statsd.CRITICAL)
        datadog.statsd.service_check(topic, datadog.statsd.OK)

    def _all_logs_to_stdout_using_flask_default_log(self):
        from sys import stdout
        self.log = logging.getLogger('werkzeug')
        self.log.setLevel(logging.INFO)
        self.log.addHandler(logging.StreamHandler(stdout))

    def _set_up_datadog(self):
        if not getenv('STATSD_SOCKET'):
            self.log.error('Missing STATSD_SOCKET environment variable, cannot report to Datadog.')
            return
        datadog.initialize(statsd_socket_path=getenv('STATSD_SOCKET'))

    def _set_up_routes(self):
        app = self.get_webserver()

        @app.route('/monitor/running')
        def monitor_running():
            return jsonify(data=True)

        @app.route('/list/result')
        def list_result():
            result = self._prepare_response_with_meta_info()
            r = self._errors
            result['data'] = r
            return jsonify(result)

        @app.route('/monitor/errors')
        def monitor_errors():
            result = self._prepare_response_with_meta_info()
            r = self._errors
            result['data'] = r
            if r:
                return jsonify(result), 503
            else:
                return jsonify(result)

    def _prepare_response_with_meta_info(self):
        return {
            'meta': {
                'obtained': self._last_update_datetime
            }
        }


def start_service():  # pragma: no cover
    service = App()
    service.start()
    return service.get_webserver()


if __name__ == '__main__':  # pragma: no cover
    webserver = start_service()
    webserver.run(host='0.0.0.0')  # Bind to all interfaces.
