import pytest
import datetime
from splinter import Browser

from app.monkey import App
from helpers.testhelpers import MockDatetime


def pytest_configure(config):
    """
    Allows plugins and conftest files to perform initial configuration.
    This hook is called for every plugin and initial conftest
    file after command line options have been parsed.
    """
    pass


def pytest_sessionstart(session):
    """
    Called after the Session object has been created and
    before performing collection and entering the run test loop.
    """
    pass


def pytest_sessionfinish(session, exitstatus):
    """
    Called after whole test run finished, right before
    returning the exit status to the system.
    """
    pass


def pytest_unconfigure(config):
    """
    called before test process is exited.
    """
    pass


@pytest.fixture
def dummy_fixture():
    pass


@pytest.fixture(scope="module")
def application():
    "Fix time for app creation."
    with MockDatetime('helpers.helpers.datetime',
                      datetime.datetime(2018, 4, 29, 9, 10, 23, 1234)):
        application = App()
    return application


@pytest.fixture()
def client(application):
    "Web client for testing JSON endpoints (resets the app)"
    application._reset()
    webserver = application.get_webserver()
    _reveal_all_errors(webserver)
    testing_client = webserver.test_client()
    _disable_before_hooks(webserver)
    ctx = webserver.app_context()
    ctx.push()
    yield testing_client
    ctx.pop()


@pytest.fixture()
def browser(application):
    "Splinter Browser for testing HTML endpoints (resets the app)"
    application._reset()
    browser = Browser('flask', app=application.get_webserver())
    yield browser
    browser.quit()


def _reveal_all_errors(flask):
    flask.config['TESTING'] = True


def _disable_before_hooks(flask):
    flask.before_first_request_funcs = []
