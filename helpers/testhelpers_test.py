import datetime
from helpers.testhelpers import MockDatetime


def test__mock_datetime():
    with MockDatetime('helpers.testhelpers_test.datetime', datetime.datetime(2019, 4, 29, 9, 10, 23, 1234)):
        assert datetime.datetime.utcnow() == datetime.datetime(2019, 4, 29, 9, 10, 23, 1234)
