import pytest
import unittest.mock as mock
import json
import datetime

from helpers.helpers import items_equal


def assert_items_equal(xs, ys):  # pragma: no cover
    assert items_equal(xs, ys)


def parse_as_json(response):
    __tracebackhide__ = True
    assert response.headers['Content-Type'] == 'application/json'
    try:
        return json.loads(response.data)
    except json.decoder.JSONDecodeError:
        pass
    pytest.fail('Expected JSON, but got {0}'.format(response.data))


class MockDatetime():
    def __init__(self, target, utcnow):
        self.target = target
        self.utcnow = utcnow

    def __enter__(self):
        self.patcher = mock.patch(self.target)
        mock_dt = self.patcher.start()
        mock_dt.datetime.utcnow.return_value = self.utcnow.replace(tzinfo=None)
        mock_dt.datetime.side_effect = lambda *args, **kw: datetime.datetime(*args, **kw)
        return mock_dt

    def __exit__(self, *args, **kwargs):
        self.patcher.stop()
