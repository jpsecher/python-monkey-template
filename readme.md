# Python Monkey Template

This repository contains a trivial worker thread and a webserver that reports the most current findings of the worker thread.  It is meant to be used as a template for an [infrastructure monkey](https://en.wikipedia.org/wiki/Chaos_engineering).

The worker thread is invoked periodically, and the webserver only reports the most current result obtained by the worker thread.

The webservice has four endpoints:

- `GET /list/result` which responds with a JSON structure that contains all the problems found by the worker thread and when the problems were found.
- `POST /run-now` forces the worker thread to start collecting data.
- `GET /` which responds with a HTML page that lists the available enddpoint.
- `GET /monitor/running` which responds with `true` so that it can be used as health endpoint.
- `GET /monitor/errors` which responds the same as `/list/result` but in addition returns a status code 200 or 503 depending on whether a problem was found or not.

The monkey assumes that the STATSD_SOCKET environment variable is pointing to a Datadog Agent socket.

## Intial local setup

To set up a local virtual Python environment:

    $ python3 -m venv ./venv

To install needed packages in the virtual environment:

    $ source ./venv/bin/activate
    $ pip install --upgrade pip
    $ pip install -r requirements-dev.txt

## Local use

Always use the virtual environment:

    $ source ./venv/bin/activate

To update the packages in the virtual environment:

    $ pip install -r requirements-dev.txt

To run the application:

    $ python app/monkey.py

To run the tests:

    $ pytest -v

To get test coverage:

    $ pytest --cov

To exit the virtual environment:

    $ deactivate

## Tests

Each module `xxx.py` is tested by `xxx_test.py`.  Each test has [three sections](http://wiki.c2.com/?ArrangeActAssert), namely *Arrange*, *Act*, and *Assert*, separated by comments.

## Theory of operation

- Bla bla
